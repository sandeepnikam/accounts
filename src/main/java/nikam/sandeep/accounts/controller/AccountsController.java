package nikam.sandeep.accounts.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.extern.slf4j.Slf4j;
import nikam.sandeep.accounts.client.CardsFeignClient;
import nikam.sandeep.accounts.config.AccountsServiceConfig;
import nikam.sandeep.accounts.dao.AccountsRepository;
import nikam.sandeep.accounts.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
public class AccountsController {


    @Autowired
    AccountsRepository accountsRepository;

    @Autowired
    AccountsServiceConfig accountsServiceConfig;

    @Autowired
    CardsFeignClient cardsFeignClient;


    @GetMapping("/my-account")
    public Accounts fetchAccountDetails(@RequestParam(value = "customerId") Integer customerId){
        log.debug("customer:{}", customerId);
        Accounts accounts = accountsRepository.getByCustomerId(customerId);
        log.debug("accounts:{}", accounts);

        return accounts;
    }

    @GetMapping("/properties")
    public Properties loadConfig() throws JsonProcessingException, CloneNotSupportedException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();


        Properties properties = new Properties(accountsServiceConfig.getMsg()
                , accountsServiceConfig.getBuildVersion()
                , accountsServiceConfig.getMailDetails()
                , accountsServiceConfig.getActiveBranches());
        return properties;
    }

    @PostMapping("/myCustomerDetails")
    CustomerDetails myCustomerDetails(@RequestBody Customer customer){
        Accounts accounts = accountsRepository.getByCustomerId(customer.getCustomerId());
        List<Cards> cards = cardsFeignClient.getCardDetails(customer);

        CustomerDetails customerDetails = new CustomerDetails();
        customerDetails.setAccounts(accounts);
        customerDetails.setCards(cards);

        return customerDetails;
    }
}
