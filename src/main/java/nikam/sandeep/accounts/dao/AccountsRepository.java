package nikam.sandeep.accounts.dao;

import nikam.sandeep.accounts.models.Accounts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountsRepository extends JpaRepository<Accounts, Long> {

    Accounts getByCustomerId(Integer customerId);
}
